package com.nabenik.service;

import javax.enterprise.inject.Default;

public class HelloService {

    public String doHello(String name){
        return "Hola camaradas ".concat(name);
    }
}
